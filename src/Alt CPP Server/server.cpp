#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <iostream>
#include <cstring>
int main(int argc, char argv[])
{

    int new_socket, statusfd, res,pos;
    int offset = 8;
    struct sockaddr_in addr;
    int addrlen = sizeof(addr);
    char buffer[1024]= {0};
    char *reply = "ACK";
    //create a tcp socket
    statusfd = socket(AF_INET, SOCK_STREAM, 0);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(8502);
    // bind to port 8501
    if (bind(statusfd,(struct sockaddr *)&addr, addrlen)<0)
    {
      printf("failed to bind");
      exit(1);
    }
    for(;;){
    memset(buffer, 0, sizeof(buffer));
    //listen to port 8501
    listen(statusfd, 0);

    //accept the http post
    new_socket = accept(statusfd, (struct sockaddr *)&addr, (socklen_t*)&addrlen);
    res = read(new_socket,buffer, 1024);
    //print buffer (i.e the data+header)
    //printf("%s\n",buffer);
    std::string data(buffer);
     pos = data.find("\r\n\r\n");
     pos += 4;
    //printf("%c\n",buffer[pos]);
    send(new_socket, reply ,sizeof(reply), 0);
    switch (buffer[pos]) {
      case 'A': std::cout << "Compresser ON" <<std::endl;
              break;
      case 'B': std::cout << "Low gear"<< std::endl;
              break;
      case 'C': std::cout << "High gear"<< std::endl;
              break;
      default : std::cout << "error on parse" << std::endl;
      }
    }
    return(0);
  }
