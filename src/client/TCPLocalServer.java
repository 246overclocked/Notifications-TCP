package client;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * This class implements java Socket server
 *
 * To use local servers and clients in a single main class. You MIGHT need multi-threading.
 *
 * @author SilverRayleigh
 *
 *
 */
public class TCPLocalServer {

    private static ServerSocket server;

    //define a collection. You might also use LinkedHashMap or TreeMap. It does not matter in our case.

    private static Map<String, String> map=new HashMap<String,String>();

    //create the constructor that creates "map".

    public TCPLocalServer(){

        map.put("a","success");

        map.put("b","gasLeak");

    }


    public static void createServer() throws IOException, ClassNotFoundException{

        /**
         * there is an overload of this method. For this one, there is NO need to specify the port (I set the default as 1314).
         *
         * there is no parameter or return for this method.
         */

        int port = 1314;
        server = new ServerSocket(port);

        while(true){
            System.out.println("Waiting for client request");

            Socket socket = server.accept();

            System.out.println("found a client");

            BufferedReader reader=new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println("got message");
            String message = reader.readLine();

            String messageToClient=map.getOrDefault(message, "nothing found");
            System.out.println("mapping");
            PrintWriter writer=new PrintWriter(socket.getOutputStream(),true);
            System.out.println("writer set");
            writer.println("Hi Client "+messageToClient);
            System.out.println("writing");
            System.out.println("sent a message to client");

            socket.close();

            if(message.equalsIgnoreCase("exit")) break;
        }

        System.out.println("Shutting down Socket server!!");

        server.close();
    }


    public static void createServer(int portNum) throws IOException, ClassNotFoundException{

        /**
         * this is an overload of the above method.
         * You may construct your own port number here, which means you can call this method multiple times, creating different servers
         *
         * @portNum
         *  the port Number you want for your server
         */

        int port = portNum;
        server = new ServerSocket(port);

        while(true){
            System.out.println("Waiting for client request");

            Socket socket = server.accept();

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String message = reader.readLine();

            String messageToClient=map.getOrDefault(message, "nothing found");

            PrintWriter writer = new PrintWriter(socket.getOutputStream(),true);

            writer.println("Hi Client "+messageToClient);

            if(message.equalsIgnoreCase("exit")) break;
        }

        System.out.println("Shutting down Socket server!!");

        server.close();
    }

