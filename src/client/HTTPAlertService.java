package client;


import okhttp3.*;

import java.io.IOException;

/**
 *
 * @author Silver Rayleigh
 *
 */

/**
 * @doc
 *  https://square.github.io/okhttp/3.x/okhttp/
 *  (but it is not super useful)
 *
 * @example
 *  https://github.com/square/okhttp/blob/master/samples/guide/src/main/java/okhttp3/guide/GetExample.java
 */

public class HTTPAlertService {

    //a very simple class it is. I mean it is literally only 10 lines of code.

    //specifies what data type we are sending to server

    public static final MediaType string
            = MediaType.parse("application/string; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    /**
     * @param
     *  url
     *      url of the server
     *  num
     *      string version of input int
     *
     * @return
     *
     *  String version of the response from the server
     *
     *

     */

    //method throws an input/output exception, so you must to wrap it in a try catch block

    public String postAndGet (String url, String num) throws IOException {



        //creates the values we send to the server

//        RequestBody body = RequestBody.create(string, num);

        RequestBody body = RequestBody.create(string, num);
        //alternative?
//        RequestBody body = new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("char", num)
//                .build();

//        form a request, but IS NOT responsible for sending the request

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        //post AND get from the server

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                System.out.println("failed");
                return null;
            }
             else {
                return response.body().string();

            }
            //return the string value of the response the serve returns


        }
    }
}