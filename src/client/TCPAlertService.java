package client;
import java.io.*;
import java.net.Socket;
/**
 *
 * this class uses java socket to create a client.
 *
 * @Author
 *
 *  SilverRayleigh
 *
 */
public class TCPAlertService {
    /**
     * this is a STATIC method. You do not have to instantiate this class to use this method. TCPAlertService.postAndGet() is enough.
     *
     * @Param
     *  ip
     *      ip of the server host
     *  port
     *      port of server host
     *  message
     *      what'cha wanna send
     *
    */

    public static void postAndGet(String ip, int portNum, String message) throws IOException{

        //declare the response as ServerResponse

        String ServerResponse;

        //form a connection

        Socket clientSocket = new Socket(ip, portNum);

        //form a class that outputs data to the server

        PrintWriter ToServer = new PrintWriter(clientSocket.getOutputStream());

        //form a reader that reads server output

        BufferedReader FromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        //write things to the server

        ToServer.println(message);

        //get the response

        ServerResponse = FromServer.readLine();

        //print a response to the console. Change this line.

        System.out.println("FROM SERVER: " + ServerResponse);



    }
