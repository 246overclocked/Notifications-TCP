package client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * This class uses java socket client
 *
 * @author SilverRayleigh
 *
 */
public class TCPLocalClient {
    public static void createClient(String message) throws IOException, ClassNotFoundException{
        /**
         * there is an overload of this method. For this version, there is no need to specify port. 1314 will be automatically used
         * @Params
         *  message
         *      the message you want to pass to the server
         *
        NOTE:
        if you want to end the connection, simply pass 'exit' as message

         */
        System.out.println("written");

        //get ip of local host
        InetAddress host = InetAddress.getLocalHost();
        System.out.println("written");
        Socket socket= new Socket(host.getHostName(), 1314);;
        System.out.println("written");
        PrintWriter writer=new PrintWriter(socket.getOutputStream(),true);
        System.out.println("written");
        writer.println(message);

        BufferedReader reader=new BufferedReader(new InputStreamReader(socket.getInputStream()));
        System.out.println("written");
        String serverMessage = reader.readLine();
        System.out.println("written");
        System.out.println("Message: " + serverMessage);


    }

    public static void createClient(String message, int port) throws IOException, ClassNotFoundException{
        /**
         * overload of the above
         * @Params
         *  message
         *      the message you want to pass to the server
         *  port
         *      the port number of the server you want to communicate with
        NOTE:
        if you want to end the connection, simply pass 'exit' as message

         */
        InetAddress host = InetAddress.getLocalHost();

        Socket socket= new Socket(host.getHostName(), port);;

        PrintWriter writer=new PrintWriter(socket.getOutputStream(),true);

        writer.println(message);

        BufferedReader reader=new BufferedReader(new InputStreamReader(socket.getInputStream()));

        String serverMessage = (String) reader.readLine();

        System.out.println("Message: " + serverMessage);


        }

}